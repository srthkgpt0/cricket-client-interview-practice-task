import React from 'react';
import './Card.css';
function CardComponent({ title, players, handleDelete }) {
  return (
    <div className='card'>
      <div className='title'>
        <h4>{title}</h4>
      </div>
      <div>
        <ul>
          {players.length > 0 &&
            players.map((player, index) => (
              // Didn't have an explicit Id for each player hence using index
              <li className='list' key={index}>
                <span className={player.isCaptain ? 'highlight' : ''}>
                  {player.playerName}
                </span>
                <span onClick={() => handleDelete(player.id)}>x</span>
              </li>
            ))}
        </ul>
      </div>
    </div>
  );
}

export default CardComponent;
