import React, { useState } from 'react';
import CardComponent from './CardComponent';
import './style.css';
function Home() {
  const [players, setPlayers] = useState([]);
  const [inputDetails, setInputDetails] = useState({
    playerName: '',
    category: '',
    isCaptain: false,
  });

  const onSubmit = (e) => {
    e.preventDefault();
    setPlayers((state) => {
      let max;
      if (state.length === 0) {
        max = 0;
      } else {
        max = state[0].id;
        for (let i = 0; i < state.length; i++) {
          if (state[i].id > max) {
            max = state[i].id;
          }
        }
      }
      const inputWithId = {
        ...inputDetails,
        id: max + 1,
      };
      return [...players, { ...inputWithId }];
    });
  };
  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setInputDetails({
      ...inputDetails,
      [name]: value,
    });
  };
  const handleDelete = (id) => {
    console.log(id);
    const newPlayerList = players.filter((player) => player.id !== id);
    setPlayers([...newPlayerList]);
  };
  return (
    <div>
      <div>
        <form onSubmit={onSubmit}>
          <div className='form'>
            <div>
              <label>Player Name : </label>
              <input
                value={inputDetails.playerName}
                onChange={(e) => handleInputChange(e)}
                name='playerName'
              />
            </div>
            <div>
              <label>Category : </label>
              <select name='category' onChange={(e) => handleInputChange(e)}>
                <option value='batsman'>Batsman</option>
                <option value='bowler'>Bowler</option>
                <option value='wicketKeeper'>WicketKeeper</option>
                <option value='allRounder'>All Rounder</option>
              </select>
            </div>
            <div>
              <label>Captain? : </label>
              <input
                name='isCaptain'
                type='checkbox'
                onChange={(e) =>
                  setInputDetails({
                    ...inputDetails,
                    isCaptain: e.target.checked,
                  })
                }
              />
            </div>
            <div>
              <button type='submit'>Add</button>
            </div>
          </div>
        </form>
      </div>
      <div className='cardComponents'>
        <CardComponent
          title='Batsman'
          players={players.filter((player) => player.category === 'batsman')}
          handleDelete={handleDelete}
        />
        <CardComponent
          title='Bowler'
          players={players.filter((player) => player.category === 'bowler')}
          handleDelete={handleDelete}
        />
        <CardComponent
          title='Wicket Keeper'
          players={players.filter(
            (player) => player.category === 'wicketKeeper'
          )}
          handleDelete={handleDelete}
        />
        <CardComponent
          title='All Rounder'
          players={players.filter((player) => player.category === 'allRounder')}
          handleDelete={handleDelete}
        />
      </div>
    </div>
  );
}

export default Home;
